package exam;

import exam.core.Address;
import exam.core.Citizen;
import exam.core.Organization;
import exam.core.Street;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

public class CityN implements Serializable {

    private Set<Citizen> citizens;
    public static Map<Integer, Street> streets = new LinkedHashMap<Integer, Street>() {{
        put(0, new Street(0, "0", 10));
        put(1, new Street(1, "1", 110));
        put(2, new Street(2, "2", 1110));
        put(3, new Street(3, "3", 111110));
        put(4, new Street(4, "4", 20));
    }};

//Test constructor
    public CityN() {
        citizens = new HashSet<>();
        for (int i = 0; i < 1_500; i++) {
            citizens.add(new Citizen(i, i + 2, i + 3, i + 4, "t" + i, "ln" + i, "fn" + i, "sn" + i, "1", LocalDateTime.now(), "1", new Address("street" + i, "house" + i, "hl" + i, "corp" + i, "fl" + i, "flLit" + i), "pn" + i, "ps" + i, i, new Organization(), LocalDateTime.now(), LocalDateTime.now()));
        }
    }

    //TODO: method for filter values in citizens
    public List filterBy(String property, String expr) {

        return null;
    }

    //TODO method for sort values
    public List getSortedCitizensBy(String... properties) {

        return null;
    }

    //TODO method for request
    public List getCitizensByRequest() {

        return null;
    }

    public void addCitizen(Citizen citizen) {
        citizens.add(citizen);
    }

    public Set<Citizen> getCitizens() {
        return citizens;
    }

    public static Map<Integer, Street> getStreets() {
        return streets;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CityN{");
        sb.append("citizens=").append(citizens);
        sb.append('}');
        return sb.toString();
    }
}
