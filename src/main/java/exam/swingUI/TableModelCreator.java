package exam.swingUI;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class TableModelCreator {

    public static <T> TableModel createTableModel(Class<T> beanClass, List<T> list) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(beanClass);
            List<String> columns = new ArrayList<>();
            List<Method> getters = new ArrayList<>();
            String name;
            for (PropertyDescriptor pd : beanInfo.getPropertyDescriptors()) {
                name = pd.getName();
                if (name.equals("class")) {
                    continue;
                }
                name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
                String[] s = name.split("(?=\\p{Upper})");
                String displayName = "";
                for (String s1 : s) {
                    displayName += s1 + " ";
                }

                columns.add(displayName);
                Method m = pd.getReadMethod();
                getters.add(m);
            }

            TableModel model = new AbstractTableModel() {

                @Override
                public String getColumnName(int column) {
                    return columns.get(column);
                }

                @Override
                public int getRowCount() {
                    return list.size();
                }

                @Override
                public int getColumnCount() {
                    return columns.size();
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    try {
                        return getters.get(columnIndex).invoke(list.get(rowIndex));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            };
            return model;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static String help = "Отче наш, Иже еси на небесе́х! \n" +
            "Да святится имя Твое, да прии́дет Царствие Твое, \n" +
            "да будет воля Твоя, яко на небеси́ и на земли́.\n" +
            "Хлеб наш насущный да́ждь нам дне́сь;\n" +
            "и оста́ви нам до́лги наша, якоже и мы оставляем должнико́м нашим; \n" +
            "и не введи нас во искушение, но изба́ви нас от лукаваго.";
}