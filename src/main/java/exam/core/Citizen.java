package exam.core;


import java.io.Serializable;
import java.time.LocalDateTime;

public class Citizen implements Serializable {

    private Integer id;
    private Integer docNumber;
    private Integer docNSeries;
    private Integer docSeries;
    private String  docType;
    private String  lastName;
    private String  firstName;
    private String  secondName;
    private String  status;
    private LocalDateTime birthday;
    private String  sex;
    private Address address;
    private String  policyNumber;
    private String  policySeries;
    private Integer tin;
    private Organization organization;
    private LocalDateTime  dateIn;
    private LocalDateTime   changeDate;

    public Citizen(Integer id, Integer docNumber, Integer docNSeries, Integer docSeries, String docType, String lastName, String firstName, String secondName, String status, LocalDateTime birthday, String sex, Address address, String policyNumber, String policySeries, Integer tin, Organization organization, LocalDateTime dateIn, LocalDateTime changeDate) {
        this.id = id;
        this.docNumber = docNumber;
        this.docNSeries = docNSeries;
        this.docSeries = docSeries;
        this.docType = docType;
        this.lastName = lastName;
        this.firstName = firstName;
        this.secondName = secondName;
        this.status = status;
        this.birthday = birthday;
        this.sex = sex;
        this.address = address;
        this.policyNumber = policyNumber;
        this.policySeries = policySeries;
        this.tin = tin;
        this.organization = organization;
        this.dateIn = dateIn;
        this.changeDate = changeDate;
    }

    public Integer getId() {
        return id;
    }

    public Integer getDocNumber() {
        return docNumber;
    }

    public Integer getDocNSeries() {
        return docNSeries;
    }

    public Integer getDocSeries() {
        return docSeries;
    }

    public String getDocType() {
        return docType;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getStatus() {
        return status;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public String getSex() {
        return sex;
    }

    public Address getAddress() {
        return address;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getPolicySeries() {
        return policySeries;
    }

    public Integer getTin() {
        return tin;
    }

    public Organization getOrganization() {
        return organization;
    }

    public LocalDateTime getDateIn() {
        return dateIn;
    }

    public LocalDateTime getChangeDate() {
        return changeDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\n\nCitizen{");
          sb.append("\nid=").append(id);
        sb.append(", \ndocNumber=").append(docNumber);
        sb.append(", \ndocNSeries=").append(docNSeries);
        sb.append(", \ndocSeries=").append(docSeries);
        sb.append(", \ndocType='").append(docType).append('\'');
        sb.append(", \nlastName='").append(lastName).append('\'');
        sb.append(", \nfirstName='").append(firstName).append('\'');
        sb.append(", \nsecondName='").append(secondName).append('\'');
        sb.append(", \nstatus='").append(status).append('\'');
        sb.append(", \nbirthday=").append(birthday);
        sb.append(", \nsex='").append(sex).append('\'');
        sb.append(", \naddress=").append(String.valueOf(address));
        sb.append(", \npolicyNumber='").append(policyNumber).append('\'');
        sb.append(", \npolicySeries='").append(policySeries).append('\'');
        sb.append(", \ntin=").append(tin);
        sb.append(", \norganization=").append(String.valueOf(organization));
        sb.append(", \ndateIn=").append(dateIn);
        sb.append(", \nchangeDate=").append(changeDate);
        sb.append('}');
        return sb.toString();
    }
}
