package exam.core;

import java.io.Serializable;

public class Address implements Serializable {

    private String street;
    private String house;
    private String houseLiter;
    private String corpus;
    private String flat;
    private String flatLiter;

    public Address(String street, String house, String houseLiter, String corpus, String flat, String flatLiter) {
        this.street = street;
        this.house = house;
        this.houseLiter = houseLiter;
        this.corpus = corpus;
        this.flat = flat;
        this.flatLiter = flatLiter;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getHouseLiter() {
        return houseLiter;
    }

    public String getCorpus() {
        return corpus;
    }

    public String getFlat() {
        return flat;
    }

    public String getFlatLiter() {
        return flatLiter;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Address{");
        sb.append("street='").append(street).append('\'');
        sb.append(", house='").append(house).append('\'');
        sb.append(", houseLiter='").append(houseLiter).append('\'');
        sb.append(", corpus='").append(corpus).append('\'');
        sb.append(", flat='").append(flat).append('\'');
        sb.append(", flatLiter='").append(flatLiter).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
