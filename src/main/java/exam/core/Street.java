package exam.core;

import java.io.Serializable;

public class Street implements Serializable {

    private Integer id;
    private String name;
    private Integer length;

    public Street(Integer id, String name, Integer length) {
        this.id = id;
        this.name = name;
        this.length = length;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getLength() {
        return length;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Street{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", length=").append(length);
        sb.append('}');
        return sb.toString();
    }
}
